﻿namespace Interview.Core.Exceptions;

public class BadRequestException: Exception
{
    public BadRequestException(string command)
        :base(command)
    {
        
    }
}