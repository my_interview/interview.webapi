﻿namespace Interview.Core.Exceptions;

public class EntityNotFoundException: Exception
{
    public EntityNotFoundException(Type entityType) : base(entityType.Name)
    {

    }
}