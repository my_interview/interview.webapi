﻿namespace Interview.Core.Exceptions;

public class NoPermissionException: Exception
{
    public NoPermissionException(string command)
        :base(command)
    {
        
    }
}