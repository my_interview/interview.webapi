﻿using Interview.Core.Entities;

namespace Interview.Core.Specifications;

public class ProfileWithQuestionsAndTagsSpecification: SpecificationBase<ProfileEntity>
{
    public ProfileWithQuestionsAndTagsSpecification(Guid profileId)
        :base(profile => profile.Id == profileId)
    {
        AddInclude(profile => profile.Questions!);
        AddStringInclude($"{nameof(ProfileEntity.Questions)}.{nameof(QuestionEntity.Tags)}");
    }
}