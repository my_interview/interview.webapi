﻿using Interview.Core.Entities;

namespace Interview.Core.Specifications;

public class QuestionsSpecification: SpecificationBase<QuestionEntity>
{
    private const int _countOnPage = 10;
    
    public QuestionsSpecification(int pageNumber): base(question => true)
    {
        Skip = _countOnPage * (pageNumber - 1);
        Take = _countOnPage;
    }
}