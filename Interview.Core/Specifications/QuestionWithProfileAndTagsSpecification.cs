﻿using System.Linq.Expressions;
using Interview.Core.Entities;

namespace Interview.Core.Specifications;

public class QuestionWithProfileAndTagsSpecification: SpecificationBase<QuestionEntity>
{
    private QuestionWithProfileAndTagsSpecification(Expression<Func<QuestionEntity, bool>> criteria)
        :base(criteria)
    {
        AddInclude(question => question.Profile);
        AddInclude(question => question.Tags);
    }
    
    public QuestionWithProfileAndTagsSpecification(Guid id): this(question => question.Id == id){}
    public QuestionWithProfileAndTagsSpecification(string content): this(question => question.Content.StartsWith(content)){}
}