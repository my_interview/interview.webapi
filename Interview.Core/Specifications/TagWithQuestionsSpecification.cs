﻿using Interview.Core.Entities;

namespace Interview.Core.Specifications;

public class TagWithQuestionsSpecification: SpecificationBase<TagEntity>
{
    public TagWithQuestionsSpecification(Guid id): base(tag => tag.Id == id)
    {
        AddInclude(tag => tag.Questions!);   
    }
}