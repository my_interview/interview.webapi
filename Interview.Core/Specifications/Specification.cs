﻿using System.Linq.Expressions;
using Interview.Core.Abstractions;

namespace Interview.Core.Specifications;

public abstract class SpecificationBase<TEntity>: ISpecification<TEntity>
{
    public Expression<Func<TEntity, bool>> Criteria { get; }
    public List<Expression<Func<TEntity, IEntity>>> Includes { get; }
    public List<Expression<Func<TEntity, IEnumerable<IEntity>>>> CollectionIncludes { get; }
    public List<string> StringIncludes { get; }
    public int Skip { get; protected set; }
    public int Take { get; protected set; }

    protected SpecificationBase(Expression<Func<TEntity, bool>> criteria)
    {
        Includes = new ();
        CollectionIncludes = new ();
        StringIncludes = new();
        Criteria = criteria;
    }
    
    protected void AddInclude(Expression<Func<TEntity, IEntity>> includeExpression)
    {
        Includes.Add(includeExpression);
    }
    
    protected void AddInclude(Expression<Func<TEntity, IEnumerable<IEntity>>> includeExpression)
    {
        CollectionIncludes.Add(includeExpression);
    }

    protected void AddStringInclude(string thenInclude)
    {
        StringIncludes.Add(thenInclude);
    }
}