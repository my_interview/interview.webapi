﻿using Interview.Core.Entities;

namespace Interview.Core.Specifications;

public class TagSpecification: SpecificationBase<TagEntity>
{
    public TagSpecification(): base(tag => true) { }
    public TagSpecification(IEnumerable<Guid> tags): base(tag => tags.Contains(tag.Id)) { }
}