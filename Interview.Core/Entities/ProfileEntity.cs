﻿using Interview.Core.Abstractions;

namespace Interview.Core.Entities;

public class ProfileEntity: IEntity
{
    public Guid Id { get; set; }
    public ICollection<QuestionEntity>? Questions { get; set; }
}