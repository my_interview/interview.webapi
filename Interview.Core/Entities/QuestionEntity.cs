﻿using Interview.Core.Abstractions;

namespace Interview.Core.Entities;

public class QuestionEntity: IEntity, IPaging
{
    public Guid Id { get; set; }
    public ProfileEntity Profile { get; set; } = null!;
    public string Content { get; set; } = null!;
    public string? Answer { get; set; }
    public ICollection<TagEntity> Tags { get; set; } = null!;
}