﻿using Interview.Core.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Interview.Core.Entities;

[Index(nameof(Name), IsUnique = true)]
public class TagEntity: IEntity
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public ICollection<QuestionEntity>? Questions { get; set; }
}