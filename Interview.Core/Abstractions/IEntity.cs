﻿namespace Interview.Core.Abstractions;

public interface IEntity
{
    Guid Id { get; set; }
}