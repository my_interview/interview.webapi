﻿using System.Linq.Expressions;

namespace Interview.Core.Abstractions;

public interface IRepository<T> where T: IEntity
{
    Task<Guid> AddAsync(T entity, CancellationToken cancellationToken);
    Task UpdateAsync(T entity, CancellationToken cancellationToken);
    Task RemoveAsync(T entity, CancellationToken cancellationToken);
    Task<bool> AnyAsync(ISpecification<T> spec, CancellationToken cancellationToken);
    Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken);
    Task<int> CountAsync(CancellationToken cancellationToken);
    Task<T> FindAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false);
    Task<T?> TryFindAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false);
    Task<IEnumerable<T>> FindCollectionAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false);
}