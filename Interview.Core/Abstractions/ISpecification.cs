﻿using System.Linq.Expressions;
using Interview.Core.Entities;

namespace Interview.Core.Abstractions;

public interface ISpecification<TEntity>
{
    Expression<Func<TEntity, bool>> Criteria { get; }
    List<Expression<Func<TEntity, IEntity>>> Includes { get; }
    List<Expression<Func<TEntity, IEnumerable<IEntity>>>> CollectionIncludes { get; }
    List<string> StringIncludes { get; }
    
    int Skip { get; }
    int Take { get; }
}