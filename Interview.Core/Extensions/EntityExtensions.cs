﻿using Interview.Core.Abstractions;

namespace Interview.Core.Extensions;

public static class EntityExtensions
{
    public static bool IsPagingEntity(this IEntity entity)
    {
        return entity.GetType().GetInterfaces().Contains(typeof(IPaging));
    }
}