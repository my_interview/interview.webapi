﻿namespace Interview.Core.Options;

public class ReactCorsOptions
{
    public string ReactPolicyName { get; set; } = null!;
    public string ReactPolicyOrigin { get; set; } = null!;
}