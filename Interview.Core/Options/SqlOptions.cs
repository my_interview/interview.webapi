﻿namespace Interview.Core.Options;

public class SqlOptions
{
    public string SqlConnectionString { get; set; } = null!;
}