﻿using AutoMapper;
using Interview.Application.Mapping;
using Xunit;

namespace Interview.UnitTests.Mapper;

public class AutoMapperTests
{
    [Fact]
    [Trait("Automapper", "Mapper Configuration")]
    public void MethodMissing_ApplicationProfiles_ShouldCorrectConfigured()
    {
        // Arrange
        var config = new MapperConfiguration(config 
            => config.AddProfile(new AssemblyMappingProfile()));

        // Act

        // Assert
        config.AssertConfigurationIsValid();
    }
}