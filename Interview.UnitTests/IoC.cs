﻿using System;
using Interview.Core.Abstractions;
using Interview.Data.Data;
using Interview.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Interview.UnitTests;

public class IoC
{
    private readonly IServiceProvider _serviceProvider;
    
    public IoC()
    {
        var services = new ServiceCollection();
        var dbName = Guid.NewGuid().ToString();

        services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase(dbName), ServiceLifetime.Singleton);
        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

        _serviceProvider = services.BuildServiceProvider();
    }

    public T Resolve<T>() where T: notnull => _serviceProvider.GetRequiredService<T>();
}