﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.Kernel;
using FluentAssertions;
using Interview.Core.Abstractions;
using Interview.Core.Exceptions;
using Interview.Data.Data;
using Interview.UnitTests.Specifications;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.UnitTests.Repositories;

public abstract class RepositoryBaseTests<TEntity> 
    where TEntity: class, IEntity
{
    protected readonly IRepository<TEntity> _repository;
    protected readonly ApplicationDbContext _db;
    protected readonly Fixture _fixture;
    
    public RepositoryBaseTests()
    {
        var ioc = new IoC();
        
        _repository = ioc.Resolve<IRepository<TEntity>>();
        _db = ioc.Resolve<ApplicationDbContext>();
        _fixture = new Fixture()
        {
            Behaviors = { new NullRecursionBehavior() }
        };
    }
    
    [Fact]
    public async Task AddAsync_NewEntity_ShouldBeAdded()
    {
        // Arrange
        var entity = _fixture.Create<TEntity>();

        // Act
        await _repository.AddAsync(entity, CancellationToken.None);

        // Assert
        var isAdded = await _db.Set<TEntity>().AnyAsync(x => x.Id == entity.Id);

        isAdded.Should().BeTrue();
    }
    
    [Fact]
    public async Task AddAsync_EntityAlreadyExists_ShouldThrowException()
    {
        // Arrange
        var entity = _fixture.Create<TEntity>();
        
        await _repository.AddAsync(entity, CancellationToken.None);

        // Act
        var handler = async () => await _repository.AddAsync(entity, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<ArgumentException>();
    }
    
    [Fact]
    public async Task AddAsync_NewEntityButExistedNestedEntities_ShouldBeAdded()
    {
        // Arrange
        var entity = _fixture.Create<TEntity>();
        
        await _repository.AddAsync(entity, CancellationToken.None);
        await _repository.RemoveAsync(entity, CancellationToken.None);

        // Act
        await _repository.AddAsync(entity, CancellationToken.None);

        // Assert
        var isExisted = await _db.Set<TEntity>().AnyAsync(x => x.Id == entity.Id);

        isExisted.Should().BeTrue();
    }
    
    [Fact]
    public async Task UpdateAsync_NewEntity_ShouldThrowException()
    {
        // Arrange
        var entity = _fixture.Create<TEntity>();

        // Act
        var handler = async () => await _repository.UpdateAsync(entity, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<DbUpdateException>();
    }

    [Fact]
    public async Task RemoveAsync_EntityAlreadyExists_ShouldBeRemoved()
    {
        // Arrange
        var entity = _fixture.Create<TEntity>();

        await _repository.AddAsync(entity, CancellationToken.None);

        // Act
        await _repository.RemoveAsync(entity, CancellationToken.None);

        // Assert
        var dbEntity = await _db.Set<TEntity>()
            .FirstOrDefaultAsync(x => x.Id == entity.Id);

        dbEntity.Should().BeNull();
    }

    [Fact]
    public async Task RemoveAsync_EntityNotExists_ShouldThrowException()
    {
        // Arrange
        var entity = Activator.CreateInstance<TEntity>();
        entity.Id = Guid.NewGuid();
        
        // Act
        var handler = async () 
            => await _repository.RemoveAsync(entity, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<DbUpdateConcurrencyException>();
    }

    [Fact]
    public async Task FindAsync_TargetEntitiesExists_ShouldReturnEntity()
    {
        // Arrange
        var targetEntity = _fixture.Create<TEntity>();
        var specification = new GenericFindByIdSpecification<TEntity>(targetEntity.Id);

        await _repository.AddAsync(targetEntity, CancellationToken.None);

        // Act
        var foundEntity = await _repository.FindAsync(specification, CancellationToken.None);

        // Assert
        foundEntity.Should().NotBeNull();
        foundEntity.Id.Should().Be(targetEntity.Id);
    }

    [Fact]
    public async Task FindAsync_TargetEntitiesMissing_ShouldThrowException()
    {
        // Arrange
        var specification = new GenericFindByIdSpecification<TEntity>(Guid.NewGuid());
        
        // Act
        var handler = async () => await _repository.FindAsync(specification, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<EntityNotFoundException>();
    }

    [Fact]
    public async Task TryFindAsync_TargetEntitiesExists_ShouldReturnEntity()
    {
        // Arrange
        var targetEntity = _fixture.Create<TEntity>();
        var specification = new GenericFindByIdSpecification<TEntity>(targetEntity.Id);

        await _repository.AddAsync(targetEntity, CancellationToken.None);

        // Act
        var foundEntity = await _repository.TryFindAsync(specification, CancellationToken.None);

        // Assert
        foundEntity.Should().NotBeNull();
        foundEntity!.Id.Should().Be(targetEntity.Id);
    }

    [Fact]
    public async Task TryFindAsync_TargetEntitiesMissing_ShouldReturnNull()
    {
        // Arrange
        var specification = new GenericFindByIdSpecification<TEntity>(Guid.NewGuid());

        // Act
        var foundEntity = await _repository.TryFindAsync(specification, CancellationToken.None);

        // Assert
        foundEntity.Should().BeNull();
    }
    
    [Fact]
    public async Task FindCollectionAsync_TargetEntitiesExists_ShouldReturnFilledCollection()
    {
        // Arrange
        var entities = _fixture.CreateMany<TEntity>(10).ToList();
        var targetId = entities.First().Id;
        var specification = new GenericFindByIdSpecification<TEntity>(targetId);

        await _db.Set<TEntity>().AddRangeAsync(entities);
        await _db.SaveChangesAsync();

        // Act
        var foundEntities = (await _repository.FindCollectionAsync(specification, CancellationToken.None))
            .ToList();

        // Assert
        foundEntities.Should().NotBeNull();
        foundEntities.Select(x => x.Id).Should().AllBeEquivalentTo(targetId);
    }

    [Fact]
    public async Task FindCollectionAsync_TargetEntitiesMissing_ShouldReturnEmptyCollection()
    {
        // Arrange
        var targetId = Guid.NewGuid();
        var specification = new GenericFindByIdSpecification<TEntity>(targetId);

        // Act
        var foundEntities = await _repository.FindCollectionAsync(specification, CancellationToken.None);

        // Assert
        foundEntities.Should().NotBeNull()
            .And.HaveCount(0);
    }
}