﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.UnitTests.Repositories;

public class QuestionRepositoryTests: RepositoryBaseTests<QuestionEntity>
{
    [Fact]
    public async Task FindCollectionWithTagsAndProfilesAsync_TargetQuestionsExists_ShouldReturnQuestions()
    {
        // Arrange
        var questions = _fixture.Build<QuestionEntity>()
            .Without(p => p.Tags)
            .CreateMany()
            .ToList();
        var targetId = questions.First().Id;
        var spec = new QuestionWithProfileAndTagsSpecification(targetId);

        await _db.Set<QuestionEntity>().AddRangeAsync(questions);
        await _db.SaveChangesAsync();

        // Act
        var foundQuestions = await _repository
            .FindCollectionAsync(spec, CancellationToken.None);

        // Assert
        foundQuestions.Should().NotBeNull()
            .And.HaveCount(1);
    }

    [Fact]
    public async Task FindCollectionWithTagsAndProfilesAsync_TargetQuestionsExistsWithTagsAndProfile_ShouldReturnAll()
    {
        // Arrange
        var questions = _fixture.Build<QuestionEntity>()
            .CreateMany()
            .ToList();
        var targetId = questions!.First().Id;
        var spec = new QuestionWithProfileAndTagsSpecification(targetId);
        
        await _db.Set<QuestionEntity>().AddRangeAsync(questions);
        await _db.SaveChangesAsync();
        
        // Act
        var foundQuestions = (await _repository
            .FindCollectionAsync(spec, CancellationToken.None))
            .ToList();

        // Assert
        foundQuestions.Should().NotBeNull().And.HaveCount(1);
        foundQuestions.First().Profile.Should().NotBeNull();
        foundQuestions.First().Tags.Should().NotBeNull()
            .And.HaveCountGreaterThan(0);
    }

    [Fact]
    public async Task FindCollectionWithTagsAndProfilesAsync_TargetQuestionsMissing_ShouldReturnEmptyCollection()
    {
        // Arrange
        var spec = new QuestionWithProfileAndTagsSpecification(Guid.NewGuid());
        
        // Act
        var foundQuestions = await _repository
            .FindCollectionAsync(spec, CancellationToken.None);

        // Assert
        foundQuestions.Should().NotBeNull()
            .And.BeEmpty();
    }

    [Fact]
    public async Task FindWithTagsAndProfileAsync_TargetQuestionExists_ShouldReturnQuestion()
    {
        // Arrange
        var question = _fixture.Create<QuestionEntity>();
        var spec = new QuestionWithProfileAndTagsSpecification(question.Id);

        await _db.Set<QuestionEntity>().AddAsync(question);
        await _db.SaveChangesAsync();

        // Act
        var foundQuestion = await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        foundQuestion.Should().NotBeNull();
        foundQuestion.Id.Should().Be(question.Id);
        foundQuestion.Profile.Should().NotBeNull();
        foundQuestion.Tags.Should().NotBeNull().And.NotBeEmpty();
    }

    [Fact]
    public async Task FindWithTagsAndProfileAsync_TargetQuestionMissing_ShouldThrowException()
    {
        // Arrange
        var spec = new QuestionWithProfileAndTagsSpecification(Guid.NewGuid());

        // Act
        var handler = async () => await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<EntityNotFoundException>();
    }
    
    
    [Fact]
    public async Task TryFindWithTagsAndProfileAsync_TargetQuestionExists_ShouldReturnQuestion()
    {
        // Arrange
        var question = _fixture.Create<QuestionEntity>();
        var spec = new QuestionWithProfileAndTagsSpecification(question.Id);

        await _db.Set<QuestionEntity>().AddAsync(question);
        await _db.SaveChangesAsync();

        // Act
        var foundQuestion = await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        foundQuestion.Should().NotBeNull();
        foundQuestion!.Id.Should().Be(question.Id);
        foundQuestion.Profile.Should().NotBeNull();
        foundQuestion.Tags.Should().NotBeNull().And.NotBeEmpty();
    }

    [Fact]
    public async Task TryFindWithTagsAndProfileAsync_TargetQuestionMissing_ShouldReturnNull()
    {
        // Arrange
        var spec = new QuestionWithProfileAndTagsSpecification(Guid.NewGuid());

        // Act
        var question = await _repository
            .TryFindAsync(spec, CancellationToken.None);

        // Assert
        question.Should().BeNull();
    }

    [Theory]
    [InlineData(0)]
    [InlineData(10)]
    public async Task GetQuestionsCountAsync_MaybeExistsQuestions_ShouldReturnCount(int questionsCount)
    {
        // Arrange
        var questions = _fixture.CreateMany<QuestionEntity>(questionsCount);

        await _db.Set<QuestionEntity>().AddRangeAsync(questions);
        await _db.SaveChangesAsync();
        
        // Act
        var count = await _repository.CountAsync(CancellationToken.None);

        // Assert
        count.Should().Be(questionsCount);
    }
}