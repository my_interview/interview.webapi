﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.UnitTests.Repositories;

public class TagRepositoryTests: RepositoryBaseTests<TagEntity>
{
    
    [Fact]
    public async Task FindCollectionWithQuestionsAsync_TargetTagsExists_ShouldReturnTags()
    {
        // Arrange
        var tags = _fixture
            .Build<TagEntity>()
            .Without(p => p.Questions)
            .CreateMany()
            .ToList();
        var spec = new TagWithQuestionsSpecification(tags.First().Id);

        await _db.Set<TagEntity>().AddRangeAsync(tags);
        await _db.SaveChangesAsync();

        // Act
        var foundTags = await _repository
            .FindCollectionAsync(spec, CancellationToken.None);

        // Assert
        foundTags.Should().NotBeNull()
            .And.HaveCount(1);
    }

    [Fact]
    public async Task FindCollectionWithQuestionsAsync_TargetTagsExistsWithQuestions_ShouldReturnTagsWithQuestions()
    {
        // Arrange
        var tags = _fixture.CreateMany<TagEntity>().ToList();
        var spec = new TagWithQuestionsSpecification(tags.First().Id);

        await _db.Set<TagEntity>().AddRangeAsync(tags);
        await _db.SaveChangesAsync();

        // Act
        var foundTags = (await _repository
            .FindCollectionAsync(spec, CancellationToken.None))
            .ToList();

        // Assert
        foundTags.Should().NotBeNull()
            .And.HaveCount(1);
        foundTags.First().Questions.Should().NotBeNull()
            .And.NotBeEmpty();
    }

    [Fact]
    public async Task FindCollectionWithQuestionsAsync_TargetTagsMissing_ShouldReturnEmptyCollection()
    {
        // Arrange
        var spec = new TagWithQuestionsSpecification(Guid.NewGuid());

        // Act
        var tags = await _repository
                .FindCollectionAsync(spec, CancellationToken.None);

        // Assert
        tags.Should().NotBeNull()
            .And.BeEmpty();
    }

    [Fact]
    public async Task FindWithQuestionsAsync_TargetTagExists_ShouldReturnTag()
    {
        // Arrange
        var tag = _fixture
            .Build<TagEntity>()
            .Without(p => p.Questions)
            .Create();
        var spec = new TagWithQuestionsSpecification(tag.Id);

        await _repository.AddAsync(tag, CancellationToken.None);

        // Act
        var foundTag = await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        foundTag.Should().NotBeNull();
        foundTag.Id.Should().Be(tag.Id);
    }

    [Fact]
    public async Task FindWithQuestionsAsync_TargetTagExistsWithQuestions_ShouldReturnTagWithQuestions()
    {
        // Arrange
        var tag = _fixture.Create<TagEntity>();
        var spec = new TagWithQuestionsSpecification(tag.Id);

        await _db.Set<TagEntity>().AddAsync(tag);
        await _db.SaveChangesAsync();
        
        // Act
        var foundTag = await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        foundTag.Should().NotBeNull();
        foundTag.Id.Should().Be(tag.Id);
        foundTag.Questions.Should().NotBeNull()
            .And.NotBeEmpty();
    }

    [Fact]
    public async Task FindWithQuestionsAsync_TargetTagMissing_ShouldThrowException()
    {
        // Arrange
        var spec = new TagWithQuestionsSpecification(Guid.NewGuid());
        
        // Act
        var handler = async () => await _repository
            .FindAsync(spec, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<EntityNotFoundException>();
    }
}