﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.UnitTests.Repositories;

public class ProfileRepositoryTests: RepositoryBaseTests<ProfileEntity>
{
    [Fact]
    public async Task FindWithQuestionsAndTagsAsync_ProfileExists_ShouldReturnProfile()
    {
        // Arrange
        var profile = _fixture.Build<ProfileEntity>()
            .Without(p => p.Questions)
            .Create();
        var specification = new ProfileWithQuestionsAndTagsSpecification(profile.Id);

        await _repository.AddAsync(profile, CancellationToken.None);

        // Act
        var foundProfile = await _repository
            .FindAsync(specification, CancellationToken.None);

        // Assert
        foundProfile.Should().NotBeNull();
    }

    [Fact]
    public async Task FindWithQuestionsAndTagsAsync_ProfileExistsWithQuestionsAndTags_ShouldReturnAll()
    {
        // Arrange
        var profile = _fixture.Create<ProfileEntity>();
        var specification = new ProfileWithQuestionsAndTagsSpecification(profile.Id);

        await _db.AddAsync(profile, CancellationToken.None);
        await _db.SaveChangesAsync();
        
        // Act
        var foundProfile = await _repository
            .FindAsync(specification, CancellationToken.None);

        // Assert
        foundProfile.Should().NotBeNull();
        foundProfile.Questions.Should().NotBeNull()
            .And.HaveCountGreaterThan(0);
        foundProfile.Questions!.Select(x => x.Profile)
            .Should().NotContainNulls();
    }

    [Fact]
    public async Task FindWithQuestionsAndTagsAsync_ProfileMissing_ShouldThrowException()
    {
        // Arrange
        var specification = new ProfileWithQuestionsAndTagsSpecification(Guid.NewGuid());

        // Act
        var handler = async () => await _repository
            .FindAsync(specification, CancellationToken.None);

        // Assert
        await handler.Should().ThrowAsync<EntityNotFoundException>();
    }
}