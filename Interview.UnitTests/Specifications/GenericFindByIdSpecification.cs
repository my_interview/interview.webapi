﻿using System;
using System.Linq.Expressions;
using Interview.Core.Abstractions;
using Interview.Core.Specifications;

namespace Interview.UnitTests.Specifications;

public class GenericFindByIdSpecification<T>: SpecificationBase<T>
    where T: IEntity
{
    public GenericFindByIdSpecification(Guid id)
        :base(entity => entity.Id == id)
    {
    }
}