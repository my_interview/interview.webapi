﻿using Interview.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Interview.Data.Data;

public class ApplicationDbContext : DbContext
{
    public DbSet<ProfileEntity> Profiles { get; set; }
    public DbSet<QuestionEntity> Questions { get; set; }
    public DbSet<TagEntity> Tags { get; set; }
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
        
    }
}