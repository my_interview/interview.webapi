﻿using Interview.Core.Abstractions;
using Interview.Core.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Interview.Data.Extensions;

internal static class QueryableExtensions
{
    internal static IQueryable<T> ApplySpecification<T>(this IQueryable<T> queryable, ISpecification<T> spec)
        where T: class, IEntity
    {
        var withIncludes = spec.Includes
            .Aggregate(queryable, (current, include) => current.Include(include));

        var withCollectionIncludes = spec.CollectionIncludes
            .Aggregate(withIncludes, (current, include) => current.Include(include));
        
        var withStringIncludes = spec.StringIncludes
            .Aggregate(withCollectionIncludes, (current, include) => current.Include(include));

        var isPaging = (typeof(T) as IEntity)?.IsPagingEntity() == true;

        if (isPaging)
        {
            return queryable.Skip(spec.Skip).Take(spec.Take);
        }

        return withStringIncludes;
    }
    
    internal static IQueryable<T> TryAddTracking<T>(this IQueryable<T> queryable, bool isTracking)
        where T: class, IEntity
    {
        if (isTracking == false)
        {
            return queryable.AsNoTracking();
        }

        return queryable;
    }
}