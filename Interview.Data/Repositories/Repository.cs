﻿using System.Linq.Expressions;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Data.Data;
using Interview.Data.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Interview.Data.Repositories;

public class Repository<T> : IRepository<T> where T : class, IEntity
{
    protected readonly ApplicationDbContext _db;

    public Repository(ApplicationDbContext db)
    {
        _db = db;
    }

    public async Task<Guid> AddAsync(T entity, CancellationToken cancellationToken)
    {
        var createdEntity = await _db.Set<T>().AddAsync(entity, cancellationToken);
        await _db.SaveChangesAsync(cancellationToken);

        return createdEntity.Entity.Id;
    }

    public async Task UpdateAsync(T entity, CancellationToken cancellationToken)
    {
        _db.Set<T>().Update(entity);
        
        await _db.SaveChangesAsync(cancellationToken);
    }

    public Task RemoveAsync(T entity, CancellationToken cancellationToken)
    {
        _db.Set<T>().Remove(entity);

        return _db.SaveChangesAsync(cancellationToken);
    }

    public Task<bool> AnyAsync(ISpecification<T> spec, CancellationToken cancellationToken)
    {
        return _db.Set<T>()
            .ApplySpecification(spec)
            .AnyAsync(spec.Criteria, cancellationToken);
    }

    public Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken)
    {
        return _db.Set<T>()
            .ApplySpecification(spec)
            .CountAsync(spec.Criteria, cancellationToken);
    }

    public Task<int> CountAsync(CancellationToken cancellationToken)
    {
        return _db.Set<T>()
            .CountAsync(cancellationToken);
    }

    public async Task<T> FindAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false)
    {
        var entity = await TryFindAsync(spec, cancellationToken, isTracking);

        return entity ?? throw new EntityNotFoundException(typeof(T));
    }

    public async Task<IEnumerable<T>> FindCollectionAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false)
    {
        var collection = await _db.Set<T>()
            .TryAddTracking(isTracking)
            .ApplySpecification(spec)
            .Where(spec.Criteria)
            .ToListAsync(cancellationToken);

        return collection;
    }

    public Task<T?> TryFindAsync(ISpecification<T> spec, CancellationToken cancellationToken, bool isTracking = false)
    {
        return _db.Set<T>()
            .TryAddTracking(isTracking)
            .ApplySpecification(spec)
            .FirstOrDefaultAsync(spec.Criteria, cancellationToken);
    }
}