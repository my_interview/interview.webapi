﻿using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.GetAllTags;

public record GetAllTagsQuery(): IRequest<GetAllTagsQueryResponse>;