﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetAllTags;

public class GetAllTagsQueryHandler: IRequestHandler<GetAllTagsQuery, GetAllTagsQueryResponse>
{
    private readonly IRepository<TagEntity> _tagRepository;
    private readonly IMapper _mapper;

    public GetAllTagsQueryHandler(
        IRepository<TagEntity> tagRepository,
        IMapper mapper)
    {
        _tagRepository = tagRepository;
        _mapper = mapper;
    }
    
    public async Task<GetAllTagsQueryResponse> Handle(GetAllTagsQuery request, CancellationToken cancellationToken)
    {
        var spec = new TagSpecification();
        var tags = await _tagRepository.FindCollectionAsync(spec, cancellationToken);
        var response = new GetAllTagsQueryResponse()
        {
            Tags = _mapper.Map<IEnumerable<GetAllTagsQueryTag>>(tags)
        };

        return response;
    }
}