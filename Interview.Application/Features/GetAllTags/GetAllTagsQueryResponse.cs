﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetAllTags;

public record GetAllTagsQueryResponse
{
    [JsonPropertyName("tags")]
    public IEnumerable<GetAllTagsQueryTag> Tags { get; init; } = null!;
}

public record GetAllTagsQueryTag: IMapFrom<TagEntity>
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("name")]
    public string Name { get; set; } = null!;
}