﻿using System.Text.Json.Serialization;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.GetQuestionsByPage;

public record GetQuestionsByPageQuery : IRequest<GetQuestionsByPageQueryResponse>
{
    [JsonPropertyName("page_number")]
    public int PageNumber { get; init; }
}