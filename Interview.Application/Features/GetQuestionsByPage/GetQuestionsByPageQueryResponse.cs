﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetQuestionsByPage;

public record GetQuestionsByPageQueryResponse
{
    [JsonPropertyName("questions")]
    public IEnumerable<GetQuestionsByPageQueryQuestion> Questions { get; init; } = null!;
}

public record GetQuestionsByPageQueryQuestion: IMapFrom<QuestionEntity>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;

    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
}