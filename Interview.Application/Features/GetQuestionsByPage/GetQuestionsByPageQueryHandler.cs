﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetQuestionsByPage;

public class GetQuestionsByPageQueryHandler: IRequestHandler<GetQuestionsByPageQuery, GetQuestionsByPageQueryResponse>
{
    private readonly IRepository<QuestionEntity> _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionsByPageQueryHandler(
        IRepository<QuestionEntity> questionRepository,
        IMapper mapper)
    {
        _questionRepository = questionRepository;
        _mapper = mapper;
    }
    
    public async Task<GetQuestionsByPageQueryResponse> Handle(GetQuestionsByPageQuery request, CancellationToken cancellationToken)
    {
        // TODO: перепроверить, возможно нужно выводить тэги
        var spec = new QuestionsSpecification(request.PageNumber);
        var questions = await _questionRepository.FindCollectionAsync(spec, cancellationToken);
        var response = new GetQuestionsByPageQueryResponse()
        {
            Questions = _mapper.Map<IEnumerable<GetQuestionsByPageQueryQuestion>>(questions)
        };

        return response;
    }
}