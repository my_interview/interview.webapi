﻿using FluentValidation;

namespace Interview.Application.Features.GetQuestionsByPage;

public class GetQuestionsByPageQueryValidator: AbstractValidator<GetQuestionsByPageQuery>
{
    public GetQuestionsByPageQueryValidator()
    {
        RuleFor(p => p.PageNumber)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Некорректный номер страницы");
    }
}