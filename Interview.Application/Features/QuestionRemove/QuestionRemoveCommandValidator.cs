﻿using FluentValidation;

namespace Interview.Application.Features.QuestionRemove;

public class QuestionRemoveCommandValidator: AbstractValidator<QuestionRemoveCommand>
{
    public QuestionRemoveCommandValidator()
    {
        RuleFor(p => p.ProfileId)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор профиля");

        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор вопроса");
    }
}