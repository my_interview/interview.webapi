﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.QuestionRemove;

public class QuestionRemoveCommandHandler: IRequestHandler<QuestionRemoveCommand>
{
    private readonly IRepository<QuestionEntity> _questionRepository;

    public QuestionRemoveCommandHandler(IRepository<QuestionEntity> questionRepository)
    {
        _questionRepository = questionRepository;
    }
    
    public async Task<Unit> Handle(QuestionRemoveCommand request, CancellationToken cancellationToken)
    {
        var spec = new QuestionWithProfileAndTagsSpecification(request.Id);
        var question = await _questionRepository.TryFindAsync(spec, cancellationToken);

        if (question is null)
        {
            return Unit.Value;
        }

        if (question.Profile.Id != request.ProfileId)
        {
            throw new NoPermissionException(nameof(QuestionRemoveCommand));
        }
        
        await _questionRepository.RemoveAsync(question, cancellationToken);
        
        return Unit.Value;
    }
}