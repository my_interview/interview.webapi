﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.QuestionRemove;

public record QuestionRemoveCommand : IRequest
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("profile_id")]
    public Guid ProfileId { get; init; }
}