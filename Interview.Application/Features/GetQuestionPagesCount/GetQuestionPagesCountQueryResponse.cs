﻿using System.Text.Json.Serialization;

namespace Interview.Application.Features.GetQuestionPagesCount;

public record GetQuestionPagesCountQueryResponse
{
    [JsonPropertyName("pages_count")]
    public int PagesCount { get; init; }
}