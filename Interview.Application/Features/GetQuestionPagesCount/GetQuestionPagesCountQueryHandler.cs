﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetQuestionPagesCount;

public class GetQuestionPagesCountQueryHandler: IRequestHandler<GetQuestionPagesCountQuery, GetQuestionPagesCountQueryResponse>
{
    private const int _questionsOnPage = 10;
    private readonly IRepository<QuestionEntity> _questionRepository;

    public GetQuestionPagesCountQueryHandler(IRepository<QuestionEntity> questionRepository)
    {
        _questionRepository = questionRepository;
    }
    
    public async Task<GetQuestionPagesCountQueryResponse> Handle(GetQuestionPagesCountQuery request, CancellationToken cancellationToken)
    {
        var questionsCount = await _questionRepository.CountAsync(cancellationToken);
        var pagesCount = (int) Math.Ceiling(questionsCount / (float) _questionsOnPage);

        return new GetQuestionPagesCountQueryResponse()
        {
            PagesCount = pagesCount
        };
    }
}