﻿using MediatR;

namespace Interview.Application.Features.GetQuestionPagesCount;

public record GetQuestionPagesCountQuery(): IRequest<GetQuestionPagesCountQueryResponse>;