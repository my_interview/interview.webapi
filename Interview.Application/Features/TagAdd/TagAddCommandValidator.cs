﻿using FluentValidation;

namespace Interview.Application.Features.TagAdd;

public class TagAddCommandValidator: AbstractValidator<TagAddCommand>
{
    public TagAddCommandValidator()
    {
        RuleFor(p => p.Name)
            .NotNull()
            .NotEmpty()
            .WithMessage("Нельзя добавить тэг без названия");
    }
}