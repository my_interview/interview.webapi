﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.TagAdd;

public class TagAddCommandHandler: IRequestHandler<TagAddCommand>
{
    private readonly IRepository<TagEntity> _tagRepository;

    public TagAddCommandHandler(IRepository<TagEntity> tagRepository)
    {
        _tagRepository = tagRepository;
    }
    
    public async Task<Unit> Handle(TagAddCommand request, CancellationToken cancellationToken)
    {
        var newTag = new TagEntity()
        {
            Name = request.Name
        };

        await _tagRepository.AddAsync(newTag, cancellationToken);
        
        return Unit.Value;
    }
}