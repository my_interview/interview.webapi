﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.TagAdd;

public record TagAddCommand : IRequest
{
    [JsonPropertyName("name")]
    public string Name { get; init; } = null!;
}