﻿using System.Text.Json.Serialization;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.GetTagById;

public record GetTagByIdQuery : IRequest<GetTagByIdQueryResponse>
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
}