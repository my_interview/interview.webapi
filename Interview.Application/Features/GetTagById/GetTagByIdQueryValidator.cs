﻿using FluentValidation;

namespace Interview.Application.Features.GetTagById;

public class GetTagByIdQueryValidator: AbstractValidator<GetTagByIdQuery>
{
    public GetTagByIdQueryValidator()
    {
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорретный идентификатор");
    }
}