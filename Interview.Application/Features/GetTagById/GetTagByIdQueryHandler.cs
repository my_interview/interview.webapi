﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetTagById;

public class GetTagByIdQueryHandler: IRequestHandler<GetTagByIdQuery, GetTagByIdQueryResponse>
{
    private readonly IRepository<TagEntity> _tagRepository;
    private readonly IMapper _mapper;

    public GetTagByIdQueryHandler(
        IRepository<TagEntity> tagRepository,
        IMapper mapper)
    {
        _tagRepository = tagRepository;
        _mapper = mapper;
    }
    
    public async Task<GetTagByIdQueryResponse> Handle(GetTagByIdQuery request, CancellationToken cancellationToken)
    {
        var spec = new TagWithQuestionsSpecification(request.Id);
        var tag = await _tagRepository.FindAsync(spec, cancellationToken);
        
        return _mapper.Map<GetTagByIdQueryResponse>(tag); 
    }
}