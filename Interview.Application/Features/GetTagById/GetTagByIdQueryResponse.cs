﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetTagById;

public record GetTagByIdQueryResponse: IMapFrom<TagEntity>
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("name")]
    public string Name { get; set; } = null!;
    
    [JsonPropertyName("questions")]
    public IEnumerable<GetTagByIdQueryQuestion>? Questions { get; init; }
}

public record GetTagByIdQueryQuestion: IMapFrom<QuestionEntity>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;

    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
}