﻿using FluentValidation;
using Interview.Application.Interfaces;
using Interview.Core.Abstractions;
using MediatR;
using MediatR.Pipeline;

namespace Interview.Application.Features;

public class MainPipeline<TRequest, TResponse>: IPipelineBehavior<TRequest, TResponse>
    where TRequest: IRequest<TResponse>
{
    private readonly IEnumerable<IValidator<TRequest>> _validators;
    private readonly IEnumerable<IPipelineLogger<TRequest, TResponse>> _loggers;

    public MainPipeline(
        IEnumerable<IValidator<TRequest>> validators,
        IEnumerable<IPipelineLogger<TRequest, TResponse>> loggers)
    {
        _validators = validators;
        _loggers = loggers;
    }
    
    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        try
        {
            foreach (var logger in _loggers)
            {
                await logger.LogBeforeHandleAsync(request, cancellationToken);
            }

            foreach (var validator in _validators)
            {
                await validator.ValidateAndThrowAsync(request, cancellationToken);
            }

            var response = await next();

            foreach (var logger in _loggers)
            {
                await logger.LogAfterHandleAsync(request, response, cancellationToken);
            }

            return response;
        }
        catch (Exception ex)
        {
            foreach (var logger in _loggers)
            {
                await logger.LogErrorAsync(request, ex, cancellationToken);
            }

            throw;
        }
    }
}