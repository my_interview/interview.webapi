﻿using Interview.Application.Interfaces;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace Interview.Application.Features;

public class PipelineLogger<TRequest, TResponse>: IPipelineLogger<TRequest, TResponse>
    where TRequest: IRequest<TResponse>
{
    private readonly ILogger<TRequest> _logger;

    public PipelineLogger(ILogger<TRequest> logger)
    {
        _logger = logger;
    }

    public Task LogBeforeHandleAsync(TRequest request, CancellationToken cancellationToken)
    {
        _logger.LogInformation("Start handling {@Request}", request);

        return Task.CompletedTask;
    }

    public Task LogAfterHandleAsync(TRequest request, TResponse response, CancellationToken cancellationToken)
    {
        _logger.LogInformation("Successfully handled {@Request} and returned response {@Response}", request, response);

        return Task.CompletedTask;
    }

    public Task LogErrorAsync(TRequest request, Exception exception, CancellationToken cancellationToken)
    {
        var exceptionConreteType = exception.GetType();
            
        _logger.LogError("In handle process {@Request} during error {@ExceptionType} with message {@ErrorMessage}", 
            request, exceptionConreteType, exception.Message);

        return Task.CompletedTask;
    }
}