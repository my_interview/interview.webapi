﻿using FluentValidation;

namespace Interview.Application.Features.QuestionAdd;

public class QuestionAddCommandValidator: AbstractValidator<QuestionAddCommand>
{
    public QuestionAddCommandValidator()
    {
        RuleFor(p => p.ProfileId)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор профиля");
        
        RuleFor(p => p.Content)
            .NotNull()
            .NotEmpty()
            .WithMessage("Нельзя добавлять вопросы без названия вопроса");

        RuleFor(p => p.Tags)
            .NotNull()
            .NotEmpty()
            .WithMessage("Нельзя добавить вопрос без тэгов");
    }
}