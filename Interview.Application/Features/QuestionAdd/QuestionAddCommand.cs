﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.QuestionAdd;

public record QuestionAddCommand : IRequest
{
    [JsonPropertyName("profile_id")]
    public Guid ProfileId { get; init; }
    
    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;
    
    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
    
    [JsonPropertyName("tags")]
    public IEnumerable<Guid> Tags { get; init; } = null!;
}