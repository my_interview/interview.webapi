﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.QuestionAdd;

public class QuestionAddCommandHandler: IRequestHandler<QuestionAddCommand>
{
    private readonly IRepository<QuestionEntity> _questionRepository;
    private readonly IRepository<TagEntity> _tagRepository;
    private readonly IRepository<ProfileEntity> _profileRepository;

    public QuestionAddCommandHandler(
        IRepository<QuestionEntity> questionRepository,
        IRepository<TagEntity> tagRepository,
        IRepository<ProfileEntity> profileRepository)
    {
        _questionRepository = questionRepository;
        _tagRepository = tagRepository;
        _profileRepository = profileRepository;
    }
    
    public async Task<Unit> Handle(QuestionAddCommand request, CancellationToken cancellationToken)
    {
        var tagSpec = new TagSpecification(request.Tags);
        var tags = await _tagRepository.FindCollectionAsync(tagSpec, cancellationToken, isTracking: true);
        
        var profileSpec = new ProfileWithQuestionsAndTagsSpecification(request.ProfileId);
        var profile = await _profileRepository.FindAsync(profileSpec, cancellationToken, isTracking: true);

        var question = new QuestionEntity()
        {
            Content = request.Content,
            Answer = request.Answer,
            Profile = profile,
            Tags = tags.ToList()
        };

        await _questionRepository.AddAsync(question, cancellationToken);
        
        return Unit.Value;
    }
}