﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.QuestionUpdate;

public record QuestionUpdateCommand : IRequest
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("profile_id")]
    public Guid ProfileId { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;
    
    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
    
    [JsonPropertyName("tags")]
    public IEnumerable<Guid> Tags { get; init; } = null!;
}