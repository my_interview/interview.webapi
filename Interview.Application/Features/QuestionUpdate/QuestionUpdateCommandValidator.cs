﻿using FluentValidation;

namespace Interview.Application.Features.QuestionUpdate;

public class QuestionUpdateCommandValidator: AbstractValidator<QuestionUpdateCommand>
{
    public QuestionUpdateCommandValidator()
    {
        RuleFor(p => p.ProfileId)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор профиля");

        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор вопроса");
            
        RuleFor(p => p.Content)
            .NotNull()
            .NotEmpty()
            .WithMessage("Не заполнено название вопроса");

        RuleFor(p => p.Tags)
            .NotNull()
            .NotEmpty()
            .WithMessage("Не добавлены тэги к вопросу");

    }
}