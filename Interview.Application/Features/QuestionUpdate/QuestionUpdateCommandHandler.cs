﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.QuestionUpdate;

public class QuestionUpdateCommandHandler: IRequestHandler<QuestionUpdateCommand>
{
    private readonly IRepository<QuestionEntity> _questionRepository;
    private readonly IRepository<TagEntity> _tagRepository;
    private readonly IMapper _mapper;

    public QuestionUpdateCommandHandler(
        IRepository<QuestionEntity> questionRepository,
        IRepository<TagEntity> tagRepository,
        IMapper mapper)
    {
        _questionRepository = questionRepository;
        _tagRepository = tagRepository;
        _mapper = mapper;
    }
    
    public async Task<Unit> Handle(QuestionUpdateCommand request, CancellationToken cancellationToken)
    {
        var spec = new QuestionWithProfileAndTagsSpecification(request.Id);
        var question = await _questionRepository.FindAsync(spec, cancellationToken, isTracking: true);

        if (question is null)
        {
            throw new EntityNotFoundException(typeof(QuestionEntity));
        }

        if (question.Profile.Id != request.ProfileId)
        {
            throw new NoPermissionException(nameof(QuestionUpdateCommand));
        }

        var tagSpec = new TagSpecification(request.Tags);
        var tags = await _tagRepository.FindCollectionAsync(tagSpec, cancellationToken, isTracking: true);

        var updatedQuestion = _mapper.Map(request, question);
        updatedQuestion.Tags = tags.ToList();

        await _questionRepository.UpdateAsync(question, cancellationToken);
        
        return Unit.Value;
    }
}