﻿using FluentValidation;

namespace Interview.Application.Features.GetQuestionsByContent;

public class GetQuestionsByContentQueryValidator: AbstractValidator<GetQuestionsByContentQuery>
{
    public GetQuestionsByContentQueryValidator()
    {
        RuleFor(p => p.Content)
            .NotNull()
            .NotEmpty()
            .WithMessage("Не передан контент");
    }
}