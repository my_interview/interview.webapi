﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.GetQuestionsByContent;

public record GetQuestionsByContentQuery : IRequest<GetQuestionsByContentQueryResponse>
{
    [JsonPropertyName("content")] 
    public string Content { get; init; } = null!;
}