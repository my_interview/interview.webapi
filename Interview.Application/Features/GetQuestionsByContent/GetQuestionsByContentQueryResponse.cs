﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetQuestionsByContent;

public record GetQuestionsByContentQueryResponse
{
    [JsonPropertyName("questions")]
    public IEnumerable<GetQuestionsByContentQueryQuestion> Questions { get; init; } = null!;
}

public record GetQuestionsByContentQueryQuestion: IMapFrom<QuestionEntity>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;

    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
}
