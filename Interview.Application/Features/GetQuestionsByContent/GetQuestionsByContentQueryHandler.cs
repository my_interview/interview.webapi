﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetQuestionsByContent;

public class GetQuestionsByContentQueryHandler: IRequestHandler<GetQuestionsByContentQuery, GetQuestionsByContentQueryResponse>
{
    private readonly IRepository<QuestionEntity> _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionsByContentQueryHandler(
        IRepository<QuestionEntity> questionRepository,
        IMapper mapper)
    {
        _questionRepository = questionRepository;
        _mapper = mapper;
    }
    
    public async Task<GetQuestionsByContentQueryResponse> Handle(GetQuestionsByContentQuery request, CancellationToken cancellationToken)
    {
        var spec = new QuestionWithProfileAndTagsSpecification(request.Content);
        var questions = await _questionRepository.FindCollectionAsync(spec, cancellationToken);
        var response = new GetQuestionsByContentQueryResponse()
        {
            Questions = _mapper.Map<IEnumerable<GetQuestionsByContentQueryQuestion>>(questions)
        };

        return response;
    }
}