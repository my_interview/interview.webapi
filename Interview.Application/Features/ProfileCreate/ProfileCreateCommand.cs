﻿using MediatR;

namespace Interview.Application.Features.ProfileCreate;

public record ProfileCreateCommand(): IRequest<ProfileCreateCommandResponse>;