﻿using System.Text.Json.Serialization;

namespace Interview.Application.Features.ProfileCreate;

public record ProfileCreateCommandResponse
{
    [JsonPropertyName("profile_id")]
    public Guid ProfileId { get; init; }
}