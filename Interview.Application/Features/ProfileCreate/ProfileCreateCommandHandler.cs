﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.ProfileCreate;

public class ProfileCreateCommandHandler: IRequestHandler<ProfileCreateCommand, ProfileCreateCommandResponse>
{
    private readonly IRepository<ProfileEntity> _profileRepository;

    public ProfileCreateCommandHandler(IRepository<ProfileEntity> profileRepository)
    {
        _profileRepository = profileRepository;
    }

    public async Task<ProfileCreateCommandResponse> Handle(ProfileCreateCommand _, CancellationToken cancellationToken)
    {
        var newProfile = new ProfileEntity();
        var createdProfileId = await _profileRepository.AddAsync(newProfile, cancellationToken);
        
        return new ProfileCreateCommandResponse()
        {
            ProfileId = createdProfileId
        };
    }
}