﻿using FluentValidation;

namespace Interview.Application.Features.TagRemove;

public class TagRemoveCommandValidator: AbstractValidator<TagRemoveCommand>
{
    public TagRemoveCommandValidator()
    {
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор");
    }
}