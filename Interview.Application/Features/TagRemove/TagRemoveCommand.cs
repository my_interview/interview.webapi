﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.TagRemove;

public record TagRemoveCommand : IRequest
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
}