﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Exceptions;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.TagRemove;

public class TagRemoveCommandHandler: IRequestHandler<TagRemoveCommand>
{
    private readonly IRepository<TagEntity> _tagRepository;

    public TagRemoveCommandHandler(IRepository<TagEntity> tagRepository)
    {
        _tagRepository = tagRepository;
    }
    
    public async Task<Unit> Handle(TagRemoveCommand request, CancellationToken cancellationToken)
    {
        var spec = new TagWithQuestionsSpecification(request.Id);
        var tag = await _tagRepository.TryFindAsync(spec, cancellationToken);

        if (tag is null)
        {
            return Unit.Value;
        }

        if (tag.Questions!.Count > 0) throw new BadRequestException(nameof(TagRemoveCommand));
        
        await _tagRepository.RemoveAsync(tag, cancellationToken);
        
        return Unit.Value;
    }
}