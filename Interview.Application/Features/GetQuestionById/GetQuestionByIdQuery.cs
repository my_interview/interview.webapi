﻿using System.Text.Json.Serialization;
using Interview.Core.Entities;
using MediatR;

namespace Interview.Application.Features.GetQuestionById;

public record GetQuestionByIdQuery : IRequest<GetQuestionByIdQueryResponse>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }
}