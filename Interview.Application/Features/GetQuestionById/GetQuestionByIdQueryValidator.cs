﻿using FluentValidation;

namespace Interview.Application.Features.GetQuestionById;

public class GetQuestionByIdQueryValidator: AbstractValidator<GetQuestionByIdQuery>
{
    public GetQuestionByIdQueryValidator()
    {
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор");
    }
}