﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetQuestionById;

public class GetQuestionByIdQueryHandler: IRequestHandler<GetQuestionByIdQuery, GetQuestionByIdQueryResponse>
{
    private readonly IRepository<QuestionEntity> _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionByIdQueryHandler(
        IRepository<QuestionEntity> questionRepository,
        IMapper mapper)
    {
        _questionRepository = questionRepository;
        _mapper = mapper;
    }
    
    public async Task<GetQuestionByIdQueryResponse> Handle(GetQuestionByIdQuery request, CancellationToken cancellationToken)
    {
        var spec = new QuestionWithProfileAndTagsSpecification(request.Id);
        var question = await _questionRepository.FindAsync(spec, cancellationToken);

        return _mapper.Map<GetQuestionByIdQueryResponse>(question);
    }
}