﻿using System.Text.Json.Serialization;
using AutoMapper;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetQuestionById;

public record GetQuestionByIdQueryResponse: IMapFrom<QuestionEntity>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;

    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
    
    [JsonPropertyName("tags")]
    public IEnumerable<GetQuestionByIdQueryTag>? Tags { get; init; }
}

public record GetQuestionByIdQueryTag: IMapFrom<TagEntity>
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("name")]
    public string Name { get; init; } = null!;
}