﻿using System.Text.Json.Serialization;
using Interview.Application.Mapping;
using Interview.Core.Entities;

namespace Interview.Application.Features.GetProfileById;

public record GetProfileByIdQueryResponse: IMapFrom<ProfileEntity>
{
    [JsonPropertyName("profile_id")]
    public Guid Id { get; init; }
    
    [JsonPropertyName("questions")]
    public IEnumerable<GetProfileByIdQueryQuestion>? Questions { get; init; }
}

public record GetProfileByIdQueryQuestion: IMapFrom<QuestionEntity>
{
    [JsonPropertyName("question_id")]
    public Guid Id { get; init; }

    [JsonPropertyName("content")]
    public string Content { get; init; } = null!;

    [JsonPropertyName("answer")]
    public string? Answer { get; init; }
    
    [JsonPropertyName("tags")]
    public IEnumerable<GetProfileByIdQueryTag>? Tags { get; init; }
}

public record GetProfileByIdQueryTag: IMapFrom<TagEntity>
{
    [JsonPropertyName("tag_id")]
    public Guid Id { get; init; }
}