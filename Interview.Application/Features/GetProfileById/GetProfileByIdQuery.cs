﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.GetProfileById;

public record GetProfileByIdQuery: IRequest<GetProfileByIdQueryResponse>
{
    [JsonPropertyName("profile_id")]
    public Guid Id { get; init; }
}