﻿using AutoMapper;
using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetProfileById;

public class GetProfileByIdQueryHandler: IRequestHandler<GetProfileByIdQuery, GetProfileByIdQueryResponse>
{
    private readonly IRepository<ProfileEntity> _profileRepository;
    private readonly IMapper _mapper;

    public GetProfileByIdQueryHandler(
        IRepository<ProfileEntity> profileRepository,
        IMapper mapper)
    {
        _profileRepository = profileRepository;
        _mapper = mapper;
    }
    
    public async Task<GetProfileByIdQueryResponse> Handle(GetProfileByIdQuery request, CancellationToken cancellationToken)
    {
        var spec = new ProfileWithQuestionsAndTagsSpecification(request.Id);
        var profile = await _profileRepository.FindAsync(spec, cancellationToken);
        var response = _mapper.Map<GetProfileByIdQueryResponse>(profile);

        return response;
    }
}