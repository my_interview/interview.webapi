﻿using FluentValidation;

namespace Interview.Application.Features.GetProfileById;

public class GetProfileByIdQueryValidator: AbstractValidator<GetProfileByIdQuery>
{
    public GetProfileByIdQueryValidator()
    {
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Не передан идентификатор");
    }   
}