﻿using Interview.Core.Abstractions;
using Interview.Core.Entities;
using Interview.Core.Specifications;
using MediatR;

namespace Interview.Application.Features.GetProfileExists;

public class GetProfileExistsQueryHandler: IRequestHandler<GetProfileExistsQuery, GetProfileExistsQueryResponse>
{
    private readonly IRepository<ProfileEntity> _profileRepository;

    public GetProfileExistsQueryHandler(IRepository<ProfileEntity> profileRepository)
    {
        _profileRepository = profileRepository;
    }
    
    public async Task<GetProfileExistsQueryResponse> Handle(GetProfileExistsQuery request, CancellationToken cancellationToken)
    {
        var spec = new ProfileWithQuestionsAndTagsSpecification(request.Id);
        var isExistsProfile = await _profileRepository.AnyAsync(spec, cancellationToken);

        return new GetProfileExistsQueryResponse()
        {
            IsExistsProfile = isExistsProfile
        };;
    }
}