﻿using FluentValidation;

namespace Interview.Application.Features.GetProfileExists;

public class GetProfileExistsQueryValidator: AbstractValidator<GetProfileExistsQuery>
{
    public GetProfileExistsQueryValidator()
    {
        RuleFor(p => p.Id)
            .NotEmpty()
            .WithMessage("Некорректный идентификатор");
    }   
}