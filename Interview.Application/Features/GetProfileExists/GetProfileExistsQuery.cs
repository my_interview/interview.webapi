﻿using System.Text.Json.Serialization;
using MediatR;

namespace Interview.Application.Features.GetProfileExists;

public record GetProfileExistsQuery : IRequest<GetProfileExistsQueryResponse>
{
    [JsonPropertyName("profile_id")]
    public Guid Id { get; init; }
}