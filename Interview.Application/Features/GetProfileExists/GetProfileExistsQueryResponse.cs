﻿using System.Text.Json.Serialization;

namespace Interview.Application.Features.GetProfileExists;

public class GetProfileExistsQueryResponse
{
    [JsonPropertyName("is_exists_profile")]
    public bool IsExistsProfile { get; init; }
}