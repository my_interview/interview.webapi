﻿using System.Reflection;
using AutoMapper;
using Interview.Application.Features.QuestionUpdate;
using Interview.Core.Entities;

namespace Interview.Application.Mapping;

public class AssemblyMappingProfile: Profile
{
    public AssemblyMappingProfile()
    {
        ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());

        CreateMap<QuestionUpdateCommand, QuestionEntity>()
            .ForMember(m => m.Profile, mo => mo.Ignore())
            .ForMember(m => m.Tags, mo => mo.Ignore());
    }

    private void ApplyMappingsFromAssembly(Assembly assembly)
    {
        var destinationTypes = assembly.GetExportedTypes()
            .Where(t => t.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>)))
            .ToList();

        foreach (var destinationType in destinationTypes)
        {
            var sourceType = destinationType.GetInterfaces()
                .First(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>))
                .GetGenericArguments()
                .First();
            
            CreateMap(sourceType, destinationType).MaxDepth(2);
        }
    }
}