﻿using MediatR;

namespace Interview.Application.Interfaces;

public interface IPipelineLogger<TRequest, TResponse>
    where TRequest: IRequest<TResponse>
{
    Task LogBeforeHandleAsync(TRequest request, CancellationToken cancellationToken);
    Task LogAfterHandleAsync(TRequest request, TResponse response, CancellationToken cancellationToken);
    Task LogErrorAsync(TRequest request, Exception exception, CancellationToken cancellationToken);
}