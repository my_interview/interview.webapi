﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Interview.Application.Features.GetQuestionById;
using Interview.Application.Features.QuestionAdd;
using Interview.Application.Features.QuestionRemove;
using Interview.Data.Data;
using Interview.IntegrationTests.Extensions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.IntegrationTests;

public class QuestionsControllerTests
{
    private readonly ApplicationFactory _applicationFactory;
    private readonly ApplicationDbContext _db;
    private readonly HttpClient _client;
    
    public QuestionsControllerTests()
    {
        _applicationFactory = new ApplicationFactory();
        _db = _applicationFactory.GetDbContext();
        _client = _applicationFactory.CreateClient();
    }
    
    [Fact]
    public async Task AddQuestion_ValidInputData_ShouldBeCreatedAndReturnNoContent()
    {
        // Arrange
        var question = new Fixture()
            .DisableInfiniteRecursion()
            .Build<QuestionAddCommand>()
            .Create();
        
        await _db.CreateQuestionDependencies(question);
        
        var request = new HttpRequestMessage(HttpMethod.Post, "api/v1/Questions")
        {
            Content = JsonContent.Create(question)
        };

        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        var isCreated = await _db.Questions.AnyAsync();

        isCreated.Should().BeTrue();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task AddQuestion_WithoutTags_ShouldNotBeCreatedAndReturnBadRequest()
    {
        // Arrange
        var question = new Fixture()
            .Build<QuestionAddCommand>()
            .Without(q => q.Tags)
            .Create();
        
        var request = new HttpRequestMessage(HttpMethod.Post, "api/v1/Questions")
        {
            Content = JsonContent.Create(question)
        };
        
        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        var isCreated = await _db.Questions.AnyAsync();

        isCreated.Should().BeFalse();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task AddQuestion_InvalidInputDataButWithTags_ShouldNotBeCreatedAndReturnBadRequest()
    {
        // Arrange
        var question = new QuestionAddCommand();
        var request = new HttpRequestMessage(HttpMethod.Post, "api/v1/Questions")
        {
            Content = JsonContent.Create(question)
        };
        
        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        var isCreated = await _db.Questions.AnyAsync();

        isCreated.Should().BeFalse();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task RemoveQuestion_NotExistsQuestion_ShouldReturnNotFound()
    {
        // Arrange
        var fakeQuestionId = Guid.NewGuid();
        var fakeProfileId = Guid.NewGuid();
        var question = new QuestionRemoveCommand()
        {
            Id = fakeQuestionId,
            ProfileId = fakeProfileId
        };
        var request = new HttpRequestMessage(HttpMethod.Delete, "api/v1/Questions")
        {
            Content = JsonContent.Create(question)
        };
        
        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task RemoveQuestion_ExistsQuestion_ShouldBeRemovedAndReturnNoContent()
    {
        // Arrange
        var questionEntity = await _db.CreateQuestionAsync();
        var questionRemove = new QuestionRemoveCommand()
        {
            Id = questionEntity.Id,
            ProfileId = questionEntity.Profile.Id
        };
        var request = new HttpRequestMessage(HttpMethod.Delete, $"api/v1/Questions")
        {
            Content = JsonContent.Create(questionRemove)
        };

        // Act
        var response = await _client.SendAsync(request);

        // Assert
        var isExists = _db.Questions.Any(x => x.Id == questionEntity.Id);

        isExists.Should().BeFalse();
        response.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact] public async Task RemoveQuestion_ExistsQuestionButRequestWithWrongProfileId_ShouldBeNotRemovedAndReturnBadRequest()
    {
        // Arrange
        var questionEntity = await _db.CreateQuestionAsync();
        var questionRemove = new QuestionRemoveCommand()
        {
            Id = questionEntity.Id,
            ProfileId = Guid.NewGuid()
        };
        var request = new HttpRequestMessage(HttpMethod.Delete, $"api/v1/Questions")
        {
            Content = JsonContent.Create(questionRemove)
        };

        // Act
        var response = await _client.SendAsync(request);

        // Assert
        var isExists = _db.Questions.Any(x => x.Id == questionEntity.Id);

        isExists.Should().BeTrue();
        response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Fact]
    public async Task GetById_ExistsQuestion_ShouldReturnQuestionWithStatusOk()
    {
        // Arrange
        var questionEntity = await _db.CreateQuestionAsync();
        var expectedQuestion = _applicationFactory.Resolve<IMapper>().Map<GetQuestionByIdQueryResponse>(questionEntity);
        
        // Act
        var response = await _client.GetAsync($"api/v1/Questions/{questionEntity.Id}");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        
        var content = await response.Content.ReadAsStringAsync();
        var actualQuestion = JsonSerializer.Deserialize<GetQuestionByIdQueryResponse>(content);
        
        actualQuestion.Should().BeEquivalentTo(expectedQuestion);
    }

    [Fact]
    public async Task GetById_NotExistsQuestion_ShouldReturnNotFound()
    {
        // Arrange
        var fakeQuestionId = Guid.NewGuid();
        
        // Act
        var response = await _client.GetAsync($"api/v1/Questions/{fakeQuestionId}");

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}