﻿using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Interview.Application.Features.GetProfileById;
using Interview.Application.Features.ProfileCreate;
using Interview.Core.Entities;
using Interview.Data.Data;
using Interview.IntegrationTests.Extensions;
using Xunit;

namespace Interview.IntegrationTests;

public class ProfilesControllerTests
{
    private readonly ApplicationFactory _applicationFactory;
    private readonly ApplicationDbContext _db;
    private readonly IMapper _mapper;
    private readonly HttpClient _client;

    public ProfilesControllerTests()
    {
        _applicationFactory = new ApplicationFactory();
        _db = _applicationFactory.GetDbContext();
        _mapper = _applicationFactory.Resolve<IMapper>();
        _client = _applicationFactory.CreateClient();
    }
    
    [Fact]
    public async Task CreateProfile_Default_ShouldReturnGuid()
    {
        // Arrange
        var request = new HttpRequestMessage(HttpMethod.Post, "/api/v1/profiles");
        
        // Act
        var httpResponse = await _client.SendAsync(request);
        
        // Assert
        var responseBody = await httpResponse.Content.ReadAsStringAsync();
        var deserializedResponse = JsonSerializer.Deserialize<ProfileCreateCommandResponse>(responseBody);

        httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        deserializedResponse!.ProfileId.Should().NotBe(Guid.Empty);
    }
    
    [Fact]
    public async Task CreateProfile_TwoRequests_ShouldReturnDifferentGuids()
    {
        // Arrange
        var firstRequest = new HttpRequestMessage(HttpMethod.Post, "/api/v1/profiles");
        var secondRequest = new HttpRequestMessage(HttpMethod.Post, "/api/v1/profiles");
        
        // Act
        var firstResponse = await _client.SendAsync(firstRequest);
        var secondResponse = await _client.SendAsync(secondRequest);

        // Assert
        var firstResponseBody = await firstResponse.Content.ReadAsStringAsync();
        var firstDeserializedResponse = JsonSerializer.Deserialize<ProfileCreateCommandResponse>(firstResponseBody);

        var secondResponseBody = await secondResponse.Content.ReadAsStringAsync();
        var secondDeserializedResponse = JsonSerializer.Deserialize<ProfileCreateCommandResponse>(secondResponseBody);

        firstResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        secondResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        firstDeserializedResponse!.ProfileId.Should()
            .NotBe(secondDeserializedResponse!.ProfileId);
    }

    [Fact]
    public async Task ExistsProfile_ProfileExists_ShouldReturnNoContent()
    {        
        // Arrange
        var profileEntity = new Fixture()
            .DisableInfiniteRecursion()
            .Create<ProfileEntity>();
        
        await _db.Profiles.AddAsync(profileEntity);
        await _db.SaveChangesAsync();
        
        var request = new HttpRequestMessage(HttpMethod.Head, $"/api/v1/profiles/{profileEntity.Id}");

        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task ExistsProfile_ProfileMissing_ShouldReturnNotFound()
    {
        // Arrange
        var fakeProfileId = Guid.NewGuid();
        var request = new HttpRequestMessage(HttpMethod.Head, $"/api/v1/profiles/{fakeProfileId}");
        
        // Act
        var httpResponse = await _client.SendAsync(request);

        // Assert
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
    
    [Fact]
    public async Task GetProfile_ProfileExists_ShouldReturnProfile()
    {
        // Arrange
        var profileEntity = new Fixture()
            .DisableInfiniteRecursion()
            .Create<ProfileEntity>();
        var expectedProfile = _mapper.Map<GetProfileByIdQueryResponse>(profileEntity);
        
        await _db.Profiles.AddAsync(profileEntity);
        await _db.SaveChangesAsync();

        // Act
        var httpResponse = await _client.GetAsync($"api/v1/Profiles/{profileEntity.Id}");
        
        // Assert
        var responseBody = await httpResponse.Content.ReadAsStringAsync();
        var deserializedResponse = JsonSerializer
            .Deserialize<GetProfileByIdQueryResponse>(responseBody);
        
        httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        deserializedResponse!.Should().BeEquivalentTo(expectedProfile);
    }

    [Fact]
    public async Task GetProfile_ProfileMissing_ShouldReturnNotFound()
    {
        // Arrange
        var fakeProfileId = Guid.NewGuid();
        
        // Act
        var httpResponse = await _client.GetAsync($"api/v1/Profiles/{fakeProfileId}");

        // Assert
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}