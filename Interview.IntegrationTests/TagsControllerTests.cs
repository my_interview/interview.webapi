﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Interview.Application.Features.GetAllTags;
using Interview.Application.Features.GetTagById;
using Interview.Application.Features.TagAdd;
using Interview.Core.Entities;
using Interview.Data.Data;
using Interview.IntegrationTests.Extensions;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Interview.IntegrationTests;

public class TagsControllerTests
{
    private readonly ApplicationFactory _applicationFactory;
    private readonly ApplicationDbContext _db;
    private readonly HttpClient _client;
    private readonly IMapper _mapper;
    
    public TagsControllerTests()
    {
        _applicationFactory = new ApplicationFactory();
        _db = _applicationFactory.GetDbContext();
        _client = _applicationFactory.CreateClient();
        _mapper = _applicationFactory.Resolve<IMapper>();
    }

    [Fact]
    public async Task AddTag_ValidInputData_ShouldBeCreatedAndReturnNoContent()
    {
        // Arrange
        var tag = new Fixture().Create<TagAddCommand>();
        var request = new HttpRequestMessage(HttpMethod.Post, "api/v1/Tags")
        {
            Content = JsonContent.Create(tag)
        };
        
        // Act
        var httpResponse = await _client.SendAsync(request);
        
        // Assert
        var isCreated = await _db.Tags.AnyAsync(t => t.Name == tag.Name);

        isCreated.Should().BeTrue();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task AddTag_InvalidInputData_ShouldNotCreatedAndReturnBadRequest()
    {
        // Arrange
        var request = new HttpRequestMessage(HttpMethod.Post, "api/v1/Tags")
        {
            Content = JsonContent.Create(new TagAddCommand())
        };
        
        // Act
        var httpResponse = await _client.SendAsync(request);
        
        // Assert
        var isCreated = await _db.Tags.AnyAsync();
        
        isCreated.Should().BeFalse();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task RemoveTag_TagNotExists_ShouldNotExistsAndReturnNoContent()
    {
        // Arrange
        var fakeTagId = Guid.NewGuid();
        
        // Act
        var httpResponse = await _client.DeleteAsync($"api/v1/Tags/{fakeTagId}");

        // Assert
        var isExists = await _db.Tags.AnyAsync(t => t.Id == fakeTagId);
        
        isExists.Should().BeFalse();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task RemoveTag_TagExistsWithoutQuestions_ShouldBeRemovedAndReturnNoContent()
    {
        // Arrange
        var tag = new Fixture()
            .Build<TagEntity>()
            .Without(p => p.Questions)
            .Create();
        
        await _db.Tags.AddAsync(tag);
        await _db.SaveChangesAsync();
        
        var request = new HttpRequestMessage(HttpMethod.Delete, $"api/v1/Tags/{tag.Id}");
        
        // Act
        var httpResponse = await _client.SendAsync(request);
        
        // Assert
        var isExists = await _db.Tags.AnyAsync(t => t.Id == tag.Id);

        isExists.Should().BeFalse();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
    }

    [Fact]
    public async Task RemoveTag_TagExistsWithQuestions_ShouldBeNotRemovedAndReturnBadRequest()
    {
        // Arrange
        var tag = new Fixture()
            .DisableInfiniteRecursion()
            .Create<TagEntity>();
        
        await _db.Tags.AddAsync(tag);
        await _db.SaveChangesAsync();
        
        // Act
        var httpResponse = await _client.DeleteAsync($"api/v1/Tags/{tag.Id}");
        
        // Assert
        var isExists = await _db.Tags.AnyAsync(t => t.Id == tag.Id);

        isExists.Should().BeTrue();
        httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task GetAll_TagsCountIsZero_ShouldReturnZeroTags()
    {
        // Arrange
        
        // Act
        var httpResponse = await _client.GetAsync("api/v1/Tags");

        // Assert
        var responseBody = await httpResponse.Content.ReadAsStringAsync();
        var deserializedResponse = JsonSerializer.Deserialize<GetAllTagsQueryResponse>(responseBody);
        
        httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        deserializedResponse!.Tags.Should().BeEmpty();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(3)]
    [InlineData(5)]
    public async Task GetAll_ContainsManyTags_ShouldReturnAllTagsAndStatusOk(short tagsCount)
    {
        // Arrange
        var tagFixture = new Fixture()
            .DisableInfiniteRecursion();
        var tagEntities = tagFixture.CreateMany<TagEntity>();
        var expectedTags = _mapper.Map<IEnumerable<GetAllTagsQueryTag>>(tagEntities).ToList();

        await _db.Set<TagEntity>().AddRangeAsync(tagEntities);
        await _db.SaveChangesAsync();
        
        // Act
        var httpResponse = await _client.GetAsync("api/v1/Tags");

        // Assert
        var responseBody = await httpResponse.Content.ReadAsStringAsync();
        var deserializedResponse = JsonSerializer.Deserialize<GetAllTagsQueryResponse>(responseBody);
        
        httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        deserializedResponse!.Tags.Should().NotBeNull()
            .And.HaveCount(expectedTags.Count)
            .And.BeEquivalentTo(expectedTags);
    }

    [Fact]
    public async Task GetById_TagExists_ShouldReturnTagAndStatusOk()
    {
        // Arrange
        var tagEntity = new Fixture()
            .DisableInfiniteRecursion()
            .Create<TagEntity>();
        var tag = _mapper.Map<GetTagByIdQueryResponse>(tagEntity);

        await _db.Tags.AddAsync(tagEntity);
        await _db.SaveChangesAsync();
        
        // Act
        var httpResponse = await _client.GetAsync($"api/v1/Tags/{tagEntity.Id}");

        // Assert
        var responseBody = await httpResponse.Content.ReadAsStringAsync();
        var deserializedResponse = JsonSerializer.Deserialize<GetTagByIdQueryResponse>(responseBody);
        
        httpResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        deserializedResponse.Should().BeEquivalentTo(tag);
    }

    [Fact]
    public async Task GetById_TagNotExists_ShouldReturnNotFound()
    {
        // Arrange
        var tagId = Guid.NewGuid();
        
        // Act
        var tagGetResponse = await _client.GetAsync($"api/v1/Tags/{tagId}");

        // Assert
        tagGetResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
    }
}