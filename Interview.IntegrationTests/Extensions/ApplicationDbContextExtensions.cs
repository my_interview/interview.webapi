﻿using System.Threading.Tasks;
using AutoFixture;
using Interview.Application.Features.QuestionAdd;
using Interview.Core.Entities;
using Interview.Data.Data;

namespace Interview.IntegrationTests.Extensions;

public static class ApplicationDbContextExtensions
{
    public static async Task CreateQuestionDependencies(this ApplicationDbContext db, QuestionAddCommand command)
    {
        foreach (var tagId in command.Tags)
        {
            var tag = new Fixture()
                .Build<TagEntity>()
                .With(t => t.Id, tagId)
                .Without(t => t.Questions)
                .Create();

            await db.Tags.AddAsync(tag);
        }

        var profile = new Fixture()
            .Build<ProfileEntity>()
            .With(p => p.Id, command.ProfileId)
            .Without(p => p.Questions)
            .Create();

        await db.Profiles.AddAsync(profile);
        await db.SaveChangesAsync();
    }
    
    public static async Task<QuestionEntity> CreateQuestionAsync(this ApplicationDbContext db)
    {
        var question = new Fixture()
            .DisableInfiniteRecursion()
            .Build<QuestionEntity>()
            .With(q => q.Profile)
            .With(q => q.Tags)
            .Create();
        
        await db.Questions.AddAsync(question);
        await db.SaveChangesAsync();

        return question;
    }
}