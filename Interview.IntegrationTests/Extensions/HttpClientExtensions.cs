﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Interview.IntegrationTests.Extensions;

public static class HttpClientExtensions
{
    public static async Task<string> GetResponseFromRequest(this HttpClient client, HttpMethod method, string uri, Dictionary<string, string>? dictionary = null)
    {
        var postRequest = new HttpRequestMessage(method, uri);

        if (dictionary != null)
        {
            postRequest.Content = new FormUrlEncodedContent(dictionary);
        }

        var response = await client.SendAsync(postRequest);
        response.EnsureSuccessStatusCode();

        return await response.Content.ReadAsStringAsync();
    }
}