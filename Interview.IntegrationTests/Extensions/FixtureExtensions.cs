﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using Interview.Core.Entities;

namespace Interview.IntegrationTests.Extensions;

public static class FixtureExtensions
{
    public static Fixture DisableInfiniteRecursion(this Fixture fixture)
    {
        
        fixture.Behaviors.Add(new NullRecursionBehavior());

        return fixture;
    }
}