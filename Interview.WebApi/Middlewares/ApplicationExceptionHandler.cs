﻿using System.Text.Json;
using FluentValidation;
using Interview.Core.Exceptions;
using Microsoft.AspNetCore.Diagnostics;

namespace Interview.WebApi.Middlewares;

public static class ApplicationExceptionHandler
{
    public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger, IWebHostEnvironment environment)
    {
        app.UseExceptionHandler(builder =>
        {
            builder.Run(async context =>
            {
                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (contextFeature is null) return;
                
                context.Response.ContentType = "application/json";

                if (await TryHandleCustomErrorAsync(contextFeature.Error, context.Response))
                {
                    return;
                }

                context.Response.StatusCode = StatusCodes.Status500InternalServerError;

                if (environment.IsDevelopment())
                {
                    await context.Response.WriteAsync($"Internal server error: {contextFeature.Error}");
                }
                else
                {
                    await context.Response.WriteAsync("Internal server error. Please try again later");
                }
            });
        });
    }

    private static async Task<bool> TryHandleCustomErrorAsync(Exception exception, HttpResponse response)
    {
        switch (exception)
        {
            case BadRequestException:
            case ValidationException: 
                response.StatusCode = StatusCodes.Status400BadRequest;
                break;
            case EntityNotFoundException:
                response.StatusCode = StatusCodes.Status404NotFound;
                break;
            case NoPermissionException:
                response.StatusCode = StatusCodes.Status401Unauthorized;
                break;
            default:
                return false;
        }
        
        await response.WriteAsync(JsonSerializer.Serialize(exception.Data));

        return true;
    }
}