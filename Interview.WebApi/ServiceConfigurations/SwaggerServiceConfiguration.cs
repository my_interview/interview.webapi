﻿namespace Interview.WebApi.ServiceConfigurations;

public static class SwaggerServiceConfiguration
{
    public static void Configure(IServiceCollection services)
    {
        services.AddSwaggerGen();
    }
}