﻿using Interview.Core.Options;
using Interview.Data.Data;
using Microsoft.EntityFrameworkCore;

namespace Interview.WebApi.ServiceConfigurations;

public static class DbServiceConfiguration
{
    public static void Configure(IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<ApplicationDbContext>(options
            => options.UseSqlServer(configuration.Get<SqlOptions>().SqlConnectionString));
    }
}