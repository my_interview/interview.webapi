﻿namespace Interview.WebApi.ServiceConfigurations;

public static class CorsServiceConfiguration
{
    public static void Configure(IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddDefaultPolicy(policy => policy
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );
        });
        
    // services.AddCors(options =>
    // {
    //     var reactCorsPolicy = configuration.Get<ReactCorsOptions>();
    //     
    //     options.AddPolicy(name: reactCorsPolicy.ReactPolicyName,
    //         policy  =>
    //         {
    //             policy.AllowAnyOrigin();
    //             policy.AllowAnyMethod();
    //             //policy.WithOrigins(reactCorsPolicy.ReactPolicyOrigin);
    //             policy.AllowAnyHeader();
    //         });
    // });
    }
}