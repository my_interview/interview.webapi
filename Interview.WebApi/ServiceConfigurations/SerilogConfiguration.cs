﻿using Serilog;

namespace Interview.WebApi.ServiceConfigurations;

public static class SerilogConfiguration
{
    public static void Configure(IServiceCollection services, WebApplicationBuilder builder)
    {
        builder.Host.UseSerilog((_, _, configuration) =>
        {
            configuration.WriteTo.Console();
        });
    }
}