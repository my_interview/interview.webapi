﻿using Interview.Application.Mapping;

namespace Interview.WebApi.ServiceConfigurations;

public static class MapperServiceConfiguration
{
    public static void Configure (IServiceCollection services)
    {
        services.AddAutoMapper(typeof(AssemblyMappingProfile));
    }
}