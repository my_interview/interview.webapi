﻿namespace Interview.WebApi.ServiceConfigurations;

public static class BaseServiceConfiguration
{
    public static void Configure(IServiceCollection services)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddRouting(c => c.LowercaseUrls = true);
    }
}