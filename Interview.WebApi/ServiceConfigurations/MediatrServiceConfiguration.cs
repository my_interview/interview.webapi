﻿using Interview.Application;
using Interview.Application.Features;
using MediatR;

namespace Interview.WebApi.ServiceConfigurations;

public static class MediatrServiceConfiguration
{
    public static void Configure(IServiceCollection services)
    {
        services.AddMediatR(typeof(IInterviewApplication));
        services.AddScoped(typeof(IPipelineBehavior<,>), typeof(MainPipeline<,>));
    }
}