﻿using Interview.Core.Options;

namespace Interview.WebApi.ServiceConfigurations;

public static class OptionsServiceConfiguration
{
    public static void Configure(
        IServiceCollection services, 
        ConfigurationManager configurationManager,
        IConfiguration configuration)
    {
        configurationManager.AddEnvironmentVariables()
            .AddUserSecrets<SqlOptions>();
        
        services.AddOptions<SqlOptions>()
            .Bind(configuration)
            .ValidateDataAnnotations();
    }
}