﻿using System.Reflection;
using Interview.Application;
using ServiceCollectionExtensions = FluentValidation.ServiceCollectionExtensions;

namespace Interview.WebApi.ServiceConfigurations;

public static class FluentValidationServiceConfiguration
{
    public static void Configure(IServiceCollection services)
    {
        ServiceCollectionExtensions.AddValidatorsFromAssembly(services, Assembly.GetAssembly(typeof(IInterviewApplication)));
    }
}