﻿using Interview.Core.Abstractions;
using Interview.Data.Repositories;

namespace Interview.WebApi.DependencyRegistrations;

public static class RepositoryRegistration
{
    public static void Register(IServiceCollection services)
    {
        services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
    }
}