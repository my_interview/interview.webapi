﻿namespace Interview.WebApi.DependencyRegistrations;

public static class HostedServiceRegistration
{
    public static void Register(IServiceCollection services)
    {
        services.AddHostedService<Worker>();
    }
}