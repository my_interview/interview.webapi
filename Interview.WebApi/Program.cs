using System.Text.Json;
using Interview.WebApi;
using Interview.WebApi.DependencyRegistrations;
using Interview.WebApi.Middlewares;
using Microsoft.AspNetCore.Diagnostics;
using Interview.WebApi.ServiceConfigurations;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

SerilogConfiguration.Configure(services, builder);
BaseServiceConfiguration.Configure(services);
CorsServiceConfiguration.Configure(services);
SwaggerServiceConfiguration.Configure(services);
OptionsServiceConfiguration.Configure(services, builder.Configuration, configuration);
DbServiceConfiguration.Configure(services, configuration);
MediatrServiceConfiguration.Configure(services);
FluentValidationServiceConfiguration.Configure(services);
MapperServiceConfiguration.Configure(services);

RepositoryRegistration.Register(services);
HostedServiceRegistration.Register(services);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.ConfigureExceptionHandler(app.Logger, app.Environment);

app.UseHttpsRedirection();

app.UseRouting();

app.UseCors();

app.UseAuthorization();

app.MapControllers();

app.Run();