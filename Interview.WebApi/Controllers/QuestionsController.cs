﻿using AutoMapper;
using Interview.Application.Features.GetQuestionById;
using Interview.Application.Features.GetQuestionPagesCount;
using Interview.Application.Features.GetQuestionsByContent;
using Interview.Application.Features.GetQuestionsByPage;
using Interview.Application.Features.QuestionAdd;
using Interview.Application.Features.QuestionRemove;
using Interview.Application.Features.QuestionUpdate;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Interview.WebApi.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class QuestionsController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public QuestionsController(IMediator mediator, IMapper mapper)
    {
        _mediator = mediator;
        _mapper = mapper;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> AddQuestion([FromBody] QuestionAddCommand command,
        CancellationToken cancellationToken)
    {
        _ = await _mediator.Send(command, cancellationToken);

        return NoContent();
    }

    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RemoveQuestion([FromBody] QuestionRemoveCommand command,
        CancellationToken cancellationToken)
    {
        _ = await _mediator.Send(command, cancellationToken);

        return NoContent();
    }

    [HttpGet("{id}/{ownerId?}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetById(Guid id, Guid ownerId, CancellationToken cancellationToken)
    {
        var query = new GetQuestionByIdQuery() { Id = id };
        var response = await _mediator.Send(query, cancellationToken);

        return Ok(response);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetByPage([FromQuery(Name = "page")] int pageNumber,
        CancellationToken cancellationToken)
    {
        var query = new GetQuestionsByPageQuery() { PageNumber = pageNumber };
        var response = await _mediator.Send(query, cancellationToken);

        return Ok(response);
    }

    [HttpGet("pages")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<int>> GetPageCount()
    {
        var query = new GetQuestionPagesCountQuery();
        var pagesCount = await _mediator.Send(query);

        return Ok(pagesCount);
    }

    [HttpPut]
    public async Task<ActionResult> UpdateQuestion([FromBody] QuestionUpdateCommand command,
        CancellationToken cancellationToken)
    {
        _ = await _mediator.Send(command, cancellationToken);

        return NoContent();
    }

    [HttpGet("all/{content}")]
    public async Task<IActionResult> GetByPartOfQuestion(string content, CancellationToken cancellationToken)
    {
        var query = new GetQuestionsByContentQuery() { Content = content };
        var questions = await _mediator.Send(query, cancellationToken);

        return Ok(questions);
    }
}