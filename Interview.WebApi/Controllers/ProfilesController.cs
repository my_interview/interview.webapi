﻿using AutoMapper;
using Interview.Application.Features.GetProfileById;
using Interview.Application.Features.ProfileCreate;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using GetProfileExistsQuery = Interview.Application.Features.GetProfileExists.GetProfileExistsQuery;

namespace Interview.WebApi.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class ProfilesController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public ProfilesController(IMediator mediator, IMapper mapper)
    {
        _mediator = mediator;
        _mapper = mapper;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateProfile(CancellationToken cancellationToken)
    {
        var command = new ProfileCreateCommand();
        var response = await _mediator.Send(command, cancellationToken);

        return Ok(response);
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetProfile(Guid id, CancellationToken cancellationToken)
    {
        var query = new GetProfileByIdQuery() { Id = id };
        var response = await _mediator.Send(query, cancellationToken);

        return Ok(response);
    }

    [HttpHead("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> ExistsProfile(Guid id, CancellationToken cancellationToken)
    {
        var query = new GetProfileExistsQuery() { Id = id };
        var response = await _mediator.Send(query, cancellationToken);

        return response.IsExistsProfile ? NoContent() : NotFound();
    }
}