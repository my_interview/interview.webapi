﻿using Interview.Application.Features.GetAllTags;
using Interview.Application.Features.GetTagById;
using Interview.Application.Features.TagAdd;
using Interview.Application.Features.TagRemove;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Interview.WebApi.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class TagsController : ControllerBase
{
    private readonly IMediator _mediator;

    public TagsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetById(Guid id, CancellationToken cancellationToken)
    {
        var query = new GetTagByIdQuery() { Id = id };
        var response = await _mediator.Send(query, cancellationToken);

        return Ok(response);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
    {
        var query = new GetAllTagsQuery();
        var response = await _mediator.Send(query, cancellationToken);
        
        return Ok(response);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> AddTag([FromBody] TagAddCommand command, CancellationToken cancellationToken)
    {
        _ = await _mediator.Send(command, cancellationToken);

        return NoContent();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> RemoveTag(Guid id, CancellationToken cancellationToken)
    {
        var command = new TagRemoveCommand() { Id = id };
        _ = await _mediator.Send(command, cancellationToken);
        
        return NoContent();
    }
}